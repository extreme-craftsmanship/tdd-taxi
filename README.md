# TDD @TaxiFee

## 开发环境
 - JDK11+
 
## 业务目标
李老板想要一个出租车计费系统，它的计算规则是这样的：
- 不超过8公里时每公里收费0.8元；
- 超过8公里则每公里加收50%长途费；
- 停车等待时每分钟加收0.25元；

## Tasking
- Given 出租车行驶了5公里(8公里以内)，未发生等待，When 计费，Then 收费4元
- Given 出租车恰好行驶了8公里，未发生等待，When 计费，Then 收费6.4元
- Given 出租车行驶了10公里(超过8公里)，未发生等待，When 计费，Then 收费12元
- Given 乘客刚上车还没开车(0公里)，未发生等待，When 计费，Then 收费0元
- Given 出租车行驶了5公里，等待10分钟，When 计费，Then 收费6.5元
- Given 出租车恰好行驶了8公里，等待10分钟，When 计费，Then 收费8.9元
- Given 出租车行驶了10公里，等待10分钟，When 计费，Then 收费14.5元
- Given 乘客上车后还没开车，等待10分钟，When 计费，Then 收费2.5元

## 参考资料
- [JUnit 5用户指南](https://gitee.com/liushide/junit5_cn_doc/blob/master/junit5UserGuide_zh_cn.md#https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fjunit-team%2Fjunit5-samples%2Ftree%2Fr5.0.2%2Fjunit5-gradle-consumer)
- [Gradle 用户指南](https://docs.gradle.org/current/userguide/userguide.html)
- 